#!/usr/bin/env stack
{- stack
  script
  --resolver lts-19.13
  --optimize
-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
import           Data.Foldable (foldl')
import           Data.Maybe    (mapMaybe)
import           Data.Word     (Word64)

-- | U64 is used to be able to switch types easily if needed
type U64 = Word64

-- | Absolute value of the difference that works
-- | for both signed and unsigned types
diff :: U64 -> U64 -> U64
diff x y
  | x < y = y - x
  | otherwise = x - y

maxN :: U64
maxN = 150

{- | Naively generates an infinite list of primes

>>> take 25 primes
[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97]
-}
primes :: [U64]
primes = 2 : primes' [3, 5..] where primes' (p:ps) = p : primes' (filter (\n -> n `mod` p /= 0) ps)

{- | Find primes < 150 of the form 4k + 1 for some k

>>> filteredPrimes
[5,13,17,29,37,41,53,61,73,89,97,101,109,113,137,149]
-}
filteredPrimes :: [U64]
filteredPrimes = takeWhile (< maxN) $ filter (\p -> p `mod` 4 == 1) primes

type ABPair = (U64, U64)

-- | Constructor for ABPair
abPair :: U64 -> U64 -> ABPair
abPair = curry id

{- | Extract value a from ABPair

>>> getA $ abPair 1 2
1

>>> getA $ abPair 2 1
1
-}
getA :: ABPair -> U64
getA (a, b) = min a b

-- | Values for a, b where a^2 + b^2 = 1
one :: ABPair
one = (0, 1)

{- | Multiply 2 `ABPair`s together and return `ABPairs`

>>> abPair 1 2 <|*|> abPair 2 3
[(1,8),(7,4)]

>>> abPair 1 2 <|*|> one
[(1,2)]
-}
(<|*|>) :: ABPair -> ABPair -> ABPairs
(<|*|>) (a, b) (c, d) =
  let ac = a * c
      ad = a * d
      bc = b * c
      bd = b * d
      p = abPair (diff ad bc) (ac + bd)
      q = abPair (ad + bc) (diff ac bd)
    in
      if p == q
        then [p]
        else [p, q]

type ABPairs = [ABPair]

{- | Multiply all elements in first list with all elements in the second list
     and return the resulting list

>>> [(abPair 1 2)] <|**|> [(abPair 2 3)]
[(1,8),(7,4)]
-}
(<|**|>) :: ABPairs -> ABPairs -> ABPairs
ps <|**|> qs = concat ((<|*|>) <$> ps <*> qs)

{- | Brute-force function to find all a, b where a^2 + b^2 = n

>>> findABPairs 5
[(1,2)]

>>> findABPairs 13
[(2,3)]

>>> findABPairs (5 * 13)
[(1,8),(4,7)]

-}
findABPairs :: U64 -> ABPairs
findABPairs n = mapMaybe findB $ takeWhile (\a -> snd a * 2 <= n) $ map toSquare [1..]
  where
    toSquare :: U64 -> (U64, U64)
    toSquare a = (a, a * a)
    findB :: (U64, U64) -> Maybe ABPair
    findB (a, a2) =
      let b = floor $ sqrt $ fromIntegral (n - a2)
        in
          if (b * b + a2) == n
            then Just $ abPair a b
            else Nothing

-- | Convert primes to list of ABPairs where a^2 + b^2 = p
primeABPairs :: [ABPairs]
primeABPairs = map findABPairs filteredPrimes

{- | Multiply every element of the two lists and concat the result to the first list

>>> [one] <|*++|> [abPair 1 2]
[(0,1),(1,2)]

>>> [one, abPair 1 2] <|*++|> [abPair 2 3]
[(0,1),(1,2),(2,3),(1,8),(7,4)]
-}
(<|*++|>) :: ABPairs -> ABPairs -> ABPairs
(<|*++|>) ps qs = ps ++ (ps <|**|> qs)

main :: IO ()
main = print $ sum $ map getA $ foldl' (<|*++|>) [one] primeABPairs
